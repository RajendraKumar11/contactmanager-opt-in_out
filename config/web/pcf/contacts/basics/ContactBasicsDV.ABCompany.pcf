<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <DetailViewPanel
    id="ContactBasicsDV"
    mode="ABCompany|ABCompanyVendor|ABMedicalCareOrg|ABAutoRepairShop|ABAutoTowingAgcy|ABLawFirm|ABPolicyCompany">
    <Require
      name="contact"
      type="ABContact"/>
    <Variable
      initialValue="contact.getSortedCategoryScores()"
      name="categoryScores"
      type="ABContactCategoryScore[]"/>
    <Variable
      initialValue="categoryScores != null and categoryScores.length &gt; 0"
      name="hasCategoryScores"
      type="Boolean"/>
    <Variable
      initialValue="new gw.web.ContactDetailsVendorHelper(contact)"
      name="contactDetailsVendorHelper"
      type="gw.web.ContactDetailsVendorHelper"/>
    <InputColumn>
      <Input
        boldLabel="true"
        boldValue="true"
        id="CreateStatus"
        label="displaykey.Web.ContactDetail.CreateStatus"
        value="contact.CreateStatus.DisplayName"
        visible="contact.CreateStatus != ContactCreationApprovalStatus.TC_APPROVED"/>
      <InputDivider
        visible="contact.CreateStatus != ContactCreationApprovalStatus.TC_APPROVED"/>
      <Label
        label="contact.getSubtype().DisplayName"/>
      <InputSetRef
        def="GlobalContactNameInputSet(new gw.api.name.ContactNameOwner(new gw.api.name.ABContactNameDelegate(contact as ABCompany)))"
        mode="gw.api.name.NameLocaleSettings.PCFMode"/>
      <InputSet
        visible="!(contact typeis ABCompanyVendor)">
        <PrivacyInput
          editable="true"
          encryptionExpression="(contact as ABCompany).maskTaxId(VALUE)"
          id="EIN"
          label="displaykey.Web.ContactDetail.Name.TaxID.EIN"
          value="(contact as ABCompany).TaxID"/>
      </InputSet>
      <InputSetRef
        def="TagsInputSet(contact)"/>
      <TypeKeyInput
        editable="true"
        id="VendorAvailability"
        label="displaykey.Web.ContactDetail.VendorAvailability"
        required="contact.Vendor"
        value="contact.VendorAvailability"
        visible="contact.Vendor">
        <PostOnChange/>
      </TypeKeyInput>
      <TextInput
        editable="true"
        id="VendorUnavailableMessageInput"
        label="displaykey.Web.ContactDetail.VendorUnavailableMessage"
        maxChars="255"
        value="contact.VendorUnavailableMessage"
        visible="contactDetailsVendorHelper.ShowVendorUnavailableMessage"/>
      <InputDivider/>
      <InputSetRef
        def="PrimaryAddressInputSet(contact as ABCompany)"/>
      <InputDivider
        visible="hasCategoryScores or (contact.Score != null)"/>
      <Label
        label="displaykey.Web.Reviews.ReviewPerformance"
        visible="hasCategoryScores or (contact.Score != null)"/>
      <Input
        id="score"
        label="displaykey.Web.Reviews.OverallScore"
        value="contact.Score"
        visible="contact.Score != null"/>
      <ListViewInput
        label="displaykey.Web.Reviews.CategoryScores"
        visible="hasCategoryScores">
        <Toolbar/>
        <ListViewPanel
          id="CategoryScoresLV">
          <RowIterator
            editable="false"
            elementName="categoryScore"
            pageSize="0"
            value="categoryScores">
            <Row>
              <TypeKeyCell
                enableSort="false"
                id="Category"
                label="displaykey.Web.Reviews.Category"
                value="categoryScore.ReviewCategory"/>
              <Cell
                enableSort="false"
                id="Score"
                label="displaykey.Web.Reviews.Score"
                value="categoryScore.Score"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputColumn>
    <InputColumn>
      <InputSet
        visible="contact typeis ABCompanyVendor">
        <InputDivider/>
        <Label
          label="displaykey.Web.ContactDetail.AdditionalInfo"/>
        <InputSetRef
          def="ABCompanyVendorBasicInputSet(contact)"
          mode="contact.Subtype"/>
        <InputSetRef
          def="ABCompanyVendorSpecialtyInputSet(contact)"
          mode="contact.Subtype"/>
      </InputSet>
      <TypeKeyInput
        editable="true"
        id="Currency"
        label="displaykey.Web.ContactDetail.PreferredCurrency"
        value="(contact as ABCompany).PreferredCurrency"
        visible="gw.api.util.CurrencyUtil.isMultiCurrencyMode()"/>
      <InputDivider/>
      <Label
        label="displaykey.Web.ContactDetail.ContactInfo"/>
      <ABContactInput
        editable="true"
        forceContactSubtype="entity.ABPerson"
        id="PrimaryContact"
        label="displaykey.Web.ContactDetail.PrimaryContact"
        newContactMenu="NewPersonOnlyPickerMenuItemSet"
        parentContact="contact"
        value="contact.PrimaryContact"/>
      <InputSetRef
        def="GlobalPhoneInputSet(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(contact, ABCompany#WorkPhone), displaykey.Web.ContactDetail.Phone.Phone, false))"
        editable="true"
        id="Work"/>
      <InputSetRef
        def="GlobalPhoneInputSet(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(contact, ABCompany#FaxPhone), displaykey.Web.ContactDetail.Phone.Fax, false))"
        editable="true"
        id="Fax"/>
      <TextInput
        editable="true"
        formatType="email"
        id="Email1"
        label="displaykey.Web.ContactDetail.PrimaryContact.Email1"
        value="(contact as ABCompany).EmailAddress1"/>
      <TextInput
        editable="true"
        formatType="email"
        id="Email2"
        label="displaykey.Web.ContactDetail.PrimaryContact.Email2"
        value="(contact as ABCompany).EmailAddress2"/>
      <InputSetRef
        def="ABCompanyVendorFormInputSet(contact)"
        mode="contact.Subtype"/>
      <InputDivider/>
      <TextAreaInput
        boldLabel="true"
        editable="true"
        id="Notes"
        label="displaykey.Web.ContactDetail.Notes"
        numRows="3"
        value="(contact as ABCompany).Notes"/>
    </InputColumn>
    <InputFooterSection>
      <InputDivider/>
      <ListViewInput
        boldLabel="true"
        def="ContactEFTLV(contact)"
        label="displaykey.Web.ContactDetail.EFT"
        labelAbove="true">
        <Toolbar>
          <IteratorButtons
            iterator="ContactEFTLV.ContactEFTLV"/>
        </Toolbar>
      </ListViewInput>
    </InputFooterSection>
  </DetailViewPanel>
</PCF>