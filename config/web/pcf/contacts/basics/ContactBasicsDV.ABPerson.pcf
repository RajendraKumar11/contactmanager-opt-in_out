<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <DetailViewPanel
    id="ContactBasicsDV"
    mode="ABPerson|ABPersonVendor|ABAdjudicator|ABUserContact|ABDoctor|ABAttorney|ABPolicyPerson">
    <Require
      name="contact"
      type="ABContact"/>
    <Variable
      initialValue="contact.CategoryScores != null and contact.CategoryScores.length &gt; 0"
      name="hasCategoryScores"
      type="Boolean"/>
    <Variable
      initialValue="new gw.web.ContactDetailsVendorHelper(contact)"
      name="contactDetailsVendorHelper"
      type="gw.web.ContactDetailsVendorHelper"/>
    <InputColumn>
      <Input
        boldLabel="true"
        boldValue="true"
        id="CreateStatus"
        label="displaykey.Web.ContactDetail.CreateStatus"
        value="contact.CreateStatus.DisplayName"
        visible="contact.CreateStatus != ContactCreationApprovalStatus.TC_APPROVED"/>
      <InputDivider
        visible="contact.CreateStatus != ContactCreationApprovalStatus.TC_APPROVED"/>
      <Label
        label="contact.getSubtype().DisplayName"/>
      <InputSetRef
        def="GlobalPersonNameInputSet(new gw.api.name.ContactNameOwner(new gw.api.name.ABPersonNameDelegate(contact as ABPerson)))"
        mode="gw.api.name.NameLocaleSettings.PCFMode"/>
      <InputSet
        visible="isPersonOnly(contact)">
        <TextInput
          editable="true"
          id="FormerName"
          label="displaykey.Web.ContactDetail.Name.FormerName"
          value="(contact as ABPerson).FormerName"/>
      </InputSet>
      <InputSetRef
        def="TagsInputSet(contact)"/>
      <TypeKeyInput
        editable="true"
        id="VendorAvailability"
        label="displaykey.Web.ContactDetail.VendorAvailability"
        required="contact.Vendor"
        value="contact.VendorAvailability"
        visible="contact.Vendor">
        <PostOnChange/>
      </TypeKeyInput>
      <TextInput
        editable="true"
        id="VendorUnavailableMessageInput"
        label="displaykey.Web.ContactDetail.VendorUnavailableMessage"
        maxChars="255"
        value="contact.VendorUnavailableMessage"
        visible="contactDetailsVendorHelper.ShowVendorUnavailableMessage"/>
      <InputDivider/>
      <InputSetRef
        def="PrimaryAddressInputSet((contact as ABPerson))"/>
      <InputDivider
        visible="hasCategoryScores or (contact.Score != null)"/>
      <Label
        label="displaykey.Web.Reviews.ReviewPerformance"
        visible="hasCategoryScores or (contact.Score != null)"/>
      <Input
        id="score"
        label="displaykey.Web.Reviews.OverallScore"
        value="contact.Score"
        visible="contact.Score != null"/>
      <ListViewInput
        label="displaykey.Web.Reviews.CategoryScores"
        visible="hasCategoryScores">
        <Toolbar/>
        <ListViewPanel
          id="CategoryScoresLV">
          <RowIterator
            editable="false"
            elementName="categoryScore"
            pageSize="0"
            value="contact.getSortedCategoryScores()">
            <Row>
              <TypeKeyCell
                enableSort="false"
                id="Category"
                label="displaykey.Web.Reviews.Category"
                value="categoryScore.ReviewCategory"/>
              <Cell
                enableSort="false"
                id="Score"
                label="displaykey.Web.Reviews.Score"
                value="categoryScore.Score"/>
            </Row>
          </RowIterator>
        </ListViewPanel>
      </ListViewInput>
    </InputColumn>
    <InputColumn>
      <Label
        label="displaykey.Web.ContactDetail.AdditionalInfo"/>
      <InputSetRef
        def="ABUserContactBasicsInputSet(contact)"
        mode="contact.Subtype"/>
      <InputSetRef
        def="ABAdjudicatorBasicsInputSet(contact)"
        mode="contact.Subtype"/>
      <TypeKeyInput
        editable="true"
        id="Currency"
        label="displaykey.Web.ContactDetail.PreferredCurrency"
        value="(contact as ABPerson).PreferredCurrency"
        visible="gw.api.util.CurrencyUtil.isMultiCurrencyMode()"/>
      <InputSet
        visible="isPersonOnly(contact) or isAdjudicator(contact) or isPolicyPerson(contact)">
        <PrivacyInput
          editable="true"
          encryptionExpression="(contact as ABPerson).maskTaxId(VALUE)"
          id="TaxID"
          label="displaykey.Web.ContactDetail.AdditionalInfo.TaxID"
          value="(contact as ABPerson).TaxID"/>
      </InputSet>
      <InputSet
        visible="isPersonOnly(contact)">
        <TypeKeyInput
          editable="true"
          id="TaxFilingStatus"
          label="displaykey.Web.ContactDetail.AdditionalInfo.TaxFilingStatus"
          value="(contact as ABPerson).TaxFilingStatus"/>
        <DateInput
          editable="true"
          id="DateOfBirth"
          label="displaykey.Web.ContactDetail.AdditionalInfo.DateOfBirth"
          value="(contact as ABPerson).DateOfBirth"/>
        <TypeKeyInput
          editable="true"
          id="Gender"
          label="displaykey.Web.ContactDetail.AdditionalInfo.Gender"
          value="(contact as ABPerson).Gender"/>
        <TypeKeyInput
          editable="true"
          id="MaritalStatus"
          label="displaykey.Web.ContactDetail.AdditionalInfo.MaritalStatus"
          value="(contact as ABPerson).MaritalStatus"/>
        <ABContactInput
          editable="true"
          forceContactSubtype="entity.ABPerson"
          id="Guardian"
          label="displaykey.Web.ContactDetail.AdditionalInfo.Guardian"
          newContactMenu="NewPersonOnlyPickerMenuItemSet"
          parentContact="contact"
          value="(contact as ABPerson).Guardian"/>
        <InputDivider/>
        <Label
          label="displaykey.Web.ContactDetail.Company"/>
        <Input
          editable="true"
          id="Occupation"
          label="displaykey.Web.ContactDetail.Company.Occupation"
          value="(contact as ABPerson).Occupation"/>
      </InputSet>
      <InputSet
        visible="isPersonOnly(contact) or isAdjudicator(contact)">
        <ABContactInput
          editable="true"
          id="Organization"
          label="displaykey.Web.ContactDetail.Company.Organization"
          parentContact="contact"
          value="(contact as ABPerson).Employer"/>
      </InputSet>
      <InputSetRef
        def="ABPersonVendorSpecialtyInputSet(contact as ABPerson)"
        mode="contact.subType"/>
      <InputSetRef
        def="ABPersonVendorInputSet(contact as ABPerson)"
        mode="contact.Subtype"/>
      <InputDivider/>
      <InputSet
        visible="isPersonOnly(contact)">
        <Label
          label="displaykey.Web.ContactDetail.DriversLicense"/>
        <Input
          editable="true"
          id="LicenseNumber"
          label="displaykey.Web.ContactDetail.DriversLicense.LicenseNumber"
          value="(contact as ABPerson).LicenseNumber"/>
        <TypeKeyInput
          editable="true"
          filter="VALUE.hasCategory(JurisdictionType.TC_DRIVING_LIC)"
          id="LicenseState"
          label="displaykey.Web.ContactDetail.DriversLicense.LicenseState"
          value="(contact as ABPerson).LicenseState"/>
        <InputDivider/>
      </InputSet>
      <TextAreaInput
        boldLabel="true"
        editable="true"
        id="Notes"
        label="displaykey.Web.ContactDetail.Notes"
        numRows="3"
        value="(contact as ABPerson).Notes"/>
    </InputColumn>
    <InputFooterSection>
      <InputDivider/>
      <ListViewInput
        boldLabel="true"
        def="ContactEFTLV(contact)"
        label="displaykey.Web.ContactDetail.EFT"
        labelAbove="true">
        <Toolbar>
          <IteratorButtons
            iterator="ContactEFTLV.ContactEFTLV"/>
        </Toolbar>
      </ListViewInput>
    </InputFooterSection>
    <Code><![CDATA[function isVendor(aContact : ABContact) : boolean {
      return aContact typeis ABPersonVendor;
      }

      function isAdjudicator(aContact : ABContact) : boolean {
      return aContact typeis ABAdjudicator;
      }

      function isPersonOnly(aContact : ABContact) : boolean {
      return aContact.Subtype=="ABPerson";
      }

      function isPolicyPerson(aContact : ABContact) : boolean {
      return aContact typeis ABPolicyPerson;
      }]]></Code>
  </DetailViewPanel>
</PCF>