package robinson.dto

uses java.util.Date
/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 4/19/16
 * Time: 3:58 PM
 * To change this template use File | Settings | File Templates.
 */
class Contact {

      var contactID           : String  as ContactID
      var name                : String  as ContactName
      var updatedDate         : Date    as UpdatedDate
      var ContactPreference   : ContactPreference as ContactPreference
      var lastName            : String as LastName
      var firstName           : String as FirstName
      var homeNumber          : String as HomeNumber
      var street              : String as Street
      var city                : String as City
      var PostCode            : String as PostalCode
      var country             : String as Country
      var primaryAddress      : String as PrimaryAddressType

}