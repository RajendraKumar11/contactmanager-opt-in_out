package robinson.batch

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 4/29/16
 * Time: 11:48 AM
 * To change this template use File | Settings | File Templates.
 */
uses gw.api.util.ConfigAccess
uses gw.processes.BatchProcessBase
uses java.io.File
uses java.lang.Exception
uses java.io.FileReader
uses java.io.BufferedReader
uses java.text.SimpleDateFormat
uses java.util.Date
uses java.lang.System
uses gw.api.system.PLConfigParameters
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses util.IntegrationPropertiesUtil
uses gw.api.database.Relop
uses org.apache.commons.lang.StringUtils
uses util.FilesUtil
uses util.common.CommonConstants
uses java.io.IOException

class RobinsonMailListBatch extends BatchProcessBase {
  override function checkInitialConditions(): boolean {
    return true
  }

  override function requestTermination(): boolean {
    return true
  }
  private static final var LOGGER: Logger = LoggerFactory.getLogger(RobinsonMailListBatch)
  private static final var file_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_file_name")
  private static final var source_File_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_source_File_path")
  private static final var targetFile = IntegrationPropertiesUtil.getProperty("RobinsonInbound_targetFile")
  private static final var client_key = IntegrationPropertiesUtil.getProperty("RobinsonInbound_client_key")
  private static final var client_separator = IntegrationPropertiesUtil.getProperty("RobinsonInbound_client_separator")
  private static final var daily_folder_pattern = IntegrationPropertiesUtil.getProperty("RobinsonInbound_file_daily_folder_pattern")
  private static final var archive_pass_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_pass_file_path")
  private static final var archive_fail_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_fail_file_path")
  private static final var ftp_archive_pass_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_source_FTPArchive_File_path")
  private static final var ftp_archive_fail_file_path = IntegrationPropertiesUtil.getProperty("RobinsonInbound_source_FTPFailed_File_path")
  private static final var date_time_format = IntegrationPropertiesUtil.getProperty("RobinsonInbound_date_time_format")
  private static final var client_smb = IntegrationPropertiesUtil.getProperty("RobinsonInbound_client_smb")
  private static final var remote_domain_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_remote_domain_name")
  private static final var archive_user_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_user_name")
  private static final var archive_password_value = IntegrationPropertiesUtil.getProperty("RobinsonInbound_archive_password_value")
  private static final var remote_user_name = IntegrationPropertiesUtil.getProperty("RobinsonInbound_remote_user_name")
  private static final var remote_password_value = IntegrationPropertiesUtil.getProperty("RobinsonInbound_remote_password_value")
  // private static final var file_directory = IntegrationPropertiesUtil.getProperty("RobinsonInbound_dir_locatiin")

  construct() {
    super(BatchProcessType.TC_ROBINSONSIMPORTBATCH)
  }

  /**
   *   Batch Process Method for  Robinsons INBOUND BATCH
   *   1. Cleans The Staging Table
   *   2. Process the File and Adds the records into DCNMInboundRecords_Ext
   *      done by Method - processSourceFile(file_name, source_File_path, targetFile)
   *   3. Runs on all the contacts and matches with the Robinsons Mailing List Records Imported data from the staging table.
   *      RobinsonInboundRec_Ext , if Match founds, Change the status accordingly Opt ins or Opt out
   */
  override function doWork() {
    LOGGER.info("RobinsonImportBatch :doWork : Begins ")
    var status = 0
    try {
      cleanTable()
     status = processSourceFile(file_name, source_File_path, targetFile)
      if(status >= 0){
        processSourceFileAndAddRecords()
      }else{
        LOGGER.error(" Cannot continue the batch process.. check logs " )
      }
    } catch (e: Exception) {
      LOGGER.error(" Exception in RobinsonImportBatch :doWork  method : " + e)
    }
    LOGGER.info("RobinsonImportBatch :doWork : Ends ")
  }

  /**
   *  Method reads flat file and update DCNMStatusInbound_Ext entity
   */
  private function updateDatabase(file: File): int {
    LOGGER.info("RobinsonImportBatch : updateDCNMStatusToDatabase : Begins ")
    var br: BufferedReader = null
    var errorStatus: int = 0
    try {
      br = new BufferedReader(new FileReader(file))
      var line = br.readLine()
      while (line != null && line != "") {
        //skipping first line of flat file as it may contains header information //no header in Robinsons,
        // line = br?.readLine()
        if (line.NotBlank)   {

          var IndexID = getRobinsonImportColumn(line, 0, 15)
          var LastName = getRobinsonImportColumn(line, 15, 50)
          var FirstName = getRobinsonImportColumn(line, 50, 65)
          var Language = getRobinsonImportColumn(line, 65, 66)
          var SexCode = getRobinsonImportColumn(line, 66, 68)
          var Title = getRobinsonImportColumn(line, 68, 98)
          var Company = getRobinsonImportColumn(line, 98, 138)
          var Street = getRobinsonImportColumn(line, 138, 178)
          var HouseNumber = getRobinsonImportColumn(line, 178, 182)
          var BoxNumber = getRobinsonImportColumn(line, 182, 186)
          var Address1 = getRobinsonImportColumn(line, 186, 216)
          var PostalCode = getRobinsonImportColumn(line, 216, 220)
          var Locality = getRobinsonImportColumn(line, 220, 252)
          var FunctionCode = getRobinsonImportColumn(line, 252, 255)
          var OriginCode = getRobinsonImportColumn(line, 255, 257)
          var IDUpdate = getRobinsonImportColumn(line, 257, 263)
          var RegistrationDate = getRobinsonImportColumn(line, 263, 271)
          var ConfirmationCode = getRobinsonImportColumn(line, 271, 272)
          var ConfirmationDate = getRobinsonImportColumn(line, 272, 280)
          var RecordNumber = getRobinsonImportColumn(line, 280, 286)


          LOGGER.debug("=================================Inserting Data Into the staging Table=====The Records len is :" + line.length)
          LOGGER.info("IndexID :" + IndexID +
              "\tLastName :" + LastName +
              "\tFirstName :" + FirstName +
              "\tLanguage :" + Language +
              "\tSexCode :" + SexCode +
              "\tTitle :" + Title +
              "\tCompany :" + Company +
              "\tStreet :" + Street +
              "\tHouseNumber:" + HouseNumber +
              "\tBoxNumber :" + BoxNumber +
              "\tAddress1 :" + Address1 +
              "\tPostalCode :" + PostalCode +
              "\tLocality:" + Locality +
              "\tFunctionCode :" + FunctionCode +
              "\tOriginCode  :" + OriginCode +
              "\tIDUpdate :" + IDUpdate +
              "\tRegistrationDate :" + RegistrationDate +
              "\tConfirmationCode :" + ConfirmationCode +
              "\tConfirmationDate :" + ConfirmationDate +
              "\tRecordNumber :" + RecordNumber)


            gw.transaction.Transaction.runWithNewBundle(\bundle -> {
                LOGGER.info("##################### Adding New Record into Staging RobinsonInboundRec_Ext Table")
                var robinsonInboundRecord = new RobinsonInboundRec_Ext()
                bundle.add(robinsonInboundRecord)

                robinsonInboundRecord.IndexID   = IndexID
                robinsonInboundRecord.LastName  = LastName
                robinsonInboundRecord.FirstName = FirstName
                robinsonInboundRecord.Language  = Language
                robinsonInboundRecord.SexCode   = SexCode
                robinsonInboundRecord.Title     = Title
                robinsonInboundRecord.Company   = Company
                robinsonInboundRecord.Street    = Street
                robinsonInboundRecord.HouseNumber = HouseNumber
                robinsonInboundRecord.BoxNumber = BoxNumber
                robinsonInboundRecord.Address1  = Address1
                robinsonInboundRecord.PostalCode    = PostalCode
                robinsonInboundRecord.Locality      = Locality
                robinsonInboundRecord.FunctionCode  = FunctionCode
                robinsonInboundRecord.OriginCode    = OriginCode
                robinsonInboundRecord.IDUpdate      = IDUpdate
                robinsonInboundRecord.RegistrationDate = RegistrationDate
                robinsonInboundRecord.ConfirmationCode = ConfirmationCode
                robinsonInboundRecord.ConfirmationDate = ConfirmationDate
                robinsonInboundRecord.RecordNumber = RecordNumber
          }, PLConfigParameters.UnrestrictedUserName.Value)
        }
        line = br?.readLine()
      }
    }
        catch (ex: Exception) {
          handleRobinsonInboundException(ex, ex.Message)
          LOGGER.error("Error while parsing and saving the flat file data into Robinsons Staging Table : " + "\\n" + ex.StackTraceAsString)
          errorStatus = - 1
        }
        finally {
      if (br != null) {
        br.close()
      }
    }
    LOGGER.info("RobinsonImportBatch : updateDatabase : Ends ")
    return errorStatus
  }

  /**
   * Increments Operations failed and log exception cause and stack trace
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  private function handleImportException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
    incrementOperationsFailed()
    OperationsFailedReasons.add(message)
  }

  /**
   *     Method load file frm remote location and copy it to local location
   *     and updated file content to database
   */
  private function processSourceFile(fileName: String, sourceFilePath: String, targetFilePath: String): int {
    LOGGER.info("RobinsonImportBatch : processSourceFile : Begins ")
    var status = 0
      var file: File = null
      try {
        LOGGER.info("RobinsonImportBatch : fileName : " + fileName + ", sourceFilePath :" + sourceFilePath + ", targetFilePath :" + targetFilePath)
        // Build source path & target path
        var sourcePath = sourceFilePath + fileName
        var targetPath = targetFilePath + fileName

        LOGGER.debug("RobinsonImportBatch : sourcePath : " + sourcePath)
        LOGGER.debug("RobinsonImportBatch : targetPath : " + targetPath)
        LOGGER.debug("RobinsonImportBatch : Env : " + java.lang.System.getProperty("gw.pc.env"))

        util.FilesUtil.nioCopyFromSourceToDestination(new File(sourcePath), new File(targetPath))

        file = new File(targetPath)

        if (file.canRead()) {
          LOGGER.debug(" RobinsonImportBatch  file can read..")

            try {
              // Update database with the imported flat file and add records to staging table
              status = updateDatabase(file)
              LOGGER.debug("RobinsonImportBatch : Add Record to DCNMStaging Entity :status " + status)
              //Operation Completed Successfully
              incrementOperationsCompleted()
            } catch (ex: Exception) {
              handleRobinsonInboundException(ex, "Could NOT update database with file  " + file.Name)
              status = -1
            }

          try {
            if (status < 0) {
              moveFilesDependingOnStatus(CommonConstants.FTP_FAILED)
              return status
            } else {
              moveFilesDependingOnStatus(CommonConstants.FTP_PROCESSED)
              return status
            }
          } catch (ex: Exception) {
             status = -1
             handleRobinsonInboundException(ex, "Exception in deleting file " + file.AbsolutePath)
          }
        } else {
              status = -1
              var message = "Error in Moving file " + file.AbsolutePath + "/"
        }
      }
    catch (ex: IOException) {
            status = -1
            handleRobinsonInboundException(ex, " Exception duringcopyFile - For file  " + fileName)
    }
    catch (ex: Exception) {
            status = -1
            handleRobinsonInboundException(ex, "Exception on processSourceFile() - Could NOT process file " + fileName)
    }

    LOGGER.info("RobinsonImportBatch : processSourceFile : Ends ")
    return status
  }

  /**
   *   converts String to Date
   *   Take param in "yyyyMMdd", and returns Java dates
   */
  private static  function convertStringToDate(dateString: String): Date {
    var formatter = new SimpleDateFormat("yyyyMMdd")
    if (dateString != null) {
      return (formatter.parse(dateString) )
    }
    else
      return null
  }

  /**
   * Process the Records which were imported into the staging table,
   * Rather iterating through the N Imported Records over GW Contacts,
   * GW contacts are compared with the imported list, If any Match found
   * the corresponding contact is marked for updating their contact preference
   *
   * If the GW Contact's, Contact preference's last source is Robinson,
   * and the imported list doesn't have a match found then it has to be marked
   *  "opt in" and status to "Default" .
   *
   */
   private function processSourceFileAndAddRecords() {
    var errorCount = 0
    LOGGER.info("Entering Method processSourceFileAndAddRecords()")
    var foundContacts = gw.api.database.Query.make(ABContact).select()
   // var contactsWithMailPreference = foundContacts.where( \ contact -> contact.ContactPreferenceOptions_Ext.hasMatch( \ elt -> elt.ContactPreferenceType == typekey.ABOptInOutPreferType_Ext.TC_MAILS))

    try {
      foundContacts?.each(\contact -> {
        var earlierOptedOutFlag = false
        LOGGER.info("processSourceFileAndAddRecords() , Searching for Contact :" + contact)
        var mailPreference = contact.ContactPreferenceOptions_Ext.firstWhere( \ elt -> elt.ContactPreferenceType == typekey.ABOptInOutPreferType_Ext.TC_MAILS)
        if(mailPreference.LastSourceOfUpdate == ABLastSourceOfUpdate_Ext.TC_ROBINSON){
            earlierOptedOutFlag = true
        }
        var matchingRecord = returnRobinsonRecordsMatchingPostCodeAndName(contact)

         if(matchingRecord!=null){
           LOGGER.debug("Iterating New MatchingRecord...")
           LOGGER.info("********* A matching Record was Found for Contact,Updating the Contact preference for : " + contact)
           LOGGER.debug("Found Contact Matching from Robinsons Mail List for GW ABContact :" +matchingRecord.DisplayName)


            updateRobinsonPreferenceOnContact(contact, matchingRecord,false,mailPreference)

           LOGGER.debug("*************Continuing with Next Record if any..")
        }
        else if(earlierOptedOutFlag && matchingRecord == null ){
        //This condition for Optin : Scnenario : Earlier contact marked out, but now cant find that matching details in
         //Robinsonlist then mark it Opt In with Default Status

           updateRobinsonPreferenceOnContact(contact, null,true,mailPreference)

         }

      })
    } catch (e: Exception) {
       errorCount = -1
      throw new Exception("Record Processing Failed RobinsonImportBatch.processSourceFileAndAddRecords()=" + e.Message)
    }
        finally {
      if (errorCount < 0) {
        moveFilesDependingOnStatus(CommonConstants.FAILED)  //Moves the file from Local Source Folder to Failed directory
      } else {
        moveFilesDependingOnStatus(CommonConstants.SUCCESS)  //Moves the file from Local Source Folder to Archived directory
      }
      LOGGER.info("Leaving Method processRecords()")
    }
  }


  /**
   * Increments Operations failed and log exception cause and stack trace
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  private function handleRobinsonInboundException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
    incrementOperationsFailed()
    OperationsFailedReasons.add(message)
  }

  /**
   *  Cleans the Staging Table
   */
  function cleanTable() {
    var robinsonsInboundRecords_Ext = gw.api.database.Query.make(RobinsonInboundRec_Ext).select()
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      robinsonsInboundRecords_Ext.each(\elt -> {
        bundle.add(elt)
        bundle.delete(elt)
      })
    }, PLConfigParameters.UnrestrictedUserName.Value)
  }

  function getDate(dateString: String): Date {
    var df = new SimpleDateFormat("yyyyMMdd")
    var cnvertCurrentDate = dateString
    var date = new java.util.Date()
    date = df.parse(cnvertCurrentDate)
    return date
  }

  function getDateTimeStringForFile(dateTimeFormat: String): String {
    var dateFormat = new SimpleDateFormat(dateTimeFormat)
    var date = new java.util.Date()
    return dateFormat.format(date)
  }
  /**
   *  Returns String in a FileName_DateTimeOfStamp format
   *  This is in order to move the files in to respective directories after processing.
   *
   */
  @Param("name", "String File Name ")
  @Param("timePart", "The Date Time Stamp")
  @Returns("FileName appended with the timestamp format as defined in Properties")
  function addTimestamp(name: String, timePart: String): String {
    var lastIndexOf: int = name.lastIndexOf('.')
    return (lastIndexOf == - 1 ? name + "_" + timePart : name.substring(0, lastIndexOf) + "_" + timePart + name.substring(lastIndexOf)).replaceAll("[\\/:\\*\\?\"<>| ]", "_")
  }

  /**
   *  Returns true if the Contact was successfully updated  with the optOut date.
   *  The contact will not be udpdated if the status is "Internal",
   *  only "Robinsons" and "Default" status values are updated
   *
   *  The Internal being the Priority , we do not over ride , if by case there is new updated OptOut date
   *  That is considered and updated for contact preference Effective Date
   */
  @Param("contact", "GW Contact which needs to be updated")
  @Param("optOutDate", "The Date of Robinsons Registration Date for the contact")
  @Returns("True if Contact was succesfully updated")
  function updateContactPreferences(contact: ABContact, optOutDate: Date,mailPreference :OptInOptOutPreference_Ext): boolean {
    LOGGER.info("In Method.updateContactPreferences()...")

    if ((mailPreference != null ) &&
        (mailPreference.LastSourceOfUpdate == null || mailPreference.LastSourceOfUpdate != ABLastSourceOfUpdate_Ext.TC_INTERNAL) &&
        ( (mailPreference.LatestUpdateDateTime?.differenceInDays(optOutDate) > 0 )
            || (mailPreference.LatestUpdateDateTime == null))) {

      gw.transaction.Transaction.runWithNewBundle(\b -> {
        b.add(mailPreference)
        mailPreference.LastSourceOfUpdate = ABLastSourceOfUpdate_Ext.TC_ROBINSON
        mailPreference.LatestUpdateDateTime = optOutDate
        mailPreference.Opt = ABContactPreferOption_Ext.TC_OPTOUT
      }, PLConfigParameters.UnrestrictedUserName.Value)
      LOGGER.info(" updateContactPreferences() Successfully Updated the contact:" + contact)
      return true
    } else {
      LOGGER.info("Debug Value : Did not find the matching contact skipping the current :" + contact.DisplayName)
      return false
    }
  }


  /**
   *  Returns true if the Contact was successfully updated  with the Optin date as Current Date.
   *  The contact will not be updated to "Default" if the Source is "Robinsons"
   *  and
   *  The Internal being the Priority , We set it back to
   */
  @Param("contact", "GW Contact which needs to be updated")
  @Param("optOutDate", "The Date of Robinsons Registration Date for the contact")
  @Returns("True if Contact was succesfully updated")
  function updateContactPreferencesWithOptIn(contact: ABContact,preference :OptInOptOutPreference_Ext): boolean {
    LOGGER.info("In Method.updateContactPreferencesWithOptIn()...")

    if ( preference.LastSourceOfUpdate == ABLastSourceOfUpdate_Ext.TC_ROBINSON ) {

      gw.transaction.Transaction.runWithNewBundle(\b -> {
        b.add(contact)
        preference.LastSourceOfUpdate = ABLastSourceOfUpdate_Ext.TC_DEFAULT
        preference.LatestUpdateDateTime = new Date()
        preference.Opt = ABContactPreferOption_Ext.TC_OPTIN
      }, PLConfigParameters.UnrestrictedUserName.Value)
      LOGGER.info(" updateContactPreferencesWithOptIn() Successfully Updated the contact:" + contact)
      return true
    } else {
      LOGGER.info("Debug Value : Did not find the matching contact and skipping the current contact :" + contact.DisplayName)
      return false
    }
  }
  /**
   *  Moves the file depending on the Status/Stages of file processing the Batch
   */
  @Param("fileStatus", "GW Contact which needs to be updated")
  private function moveFilesDependingOnStatus(fileStatus: String) {

    switch(fileStatus){

      case CommonConstants.SUCCESS : var localFile: File = new File (targetFile + file_name)
                                    if (localFile.exists()) {
                                      var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
                                      var archievedFilePath = archive_pass_file_path + File.separator + archivedFileName;
                                      FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
                                      localFile.delete()
                                    } else {
                                      LOGGER.error("Local Source File Not Found")
                                    }
                                    break


      case CommonConstants.FAILED : var localFile: File = new File (source_File_path + file_name)
                                    if (localFile.exists()) {
                                      var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
                                      var archievedFilePath = archive_fail_file_path + File.separator + archivedFileName;
                                      FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
                                      localFile.delete()
                                    }
                                    else {
                                      LOGGER.error("Local Source File Not Found")
                                    }
                                    break
      case CommonConstants.FTP_PROCESSED :
                                    var localFile: File = new File (source_File_path + file_name)
                                    if (localFile.exists()) {
                                      var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
                                      var archievedFilePath = ftp_archive_pass_file_path + File.separator + archivedFileName;
                                      FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
                                      localFile.delete()
                                    }
                                    else {
                                      LOGGER.error("Local Source File Not Found")
                                    }
                                    break
      case CommonConstants.FTP_FAILED :
                                    var localFile: File = new File (source_File_path + file_name)
                                    if (localFile.exists()) {
                                      var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
                                      var archievedFilePath = ftp_archive_fail_file_path + File.separator + archivedFileName;
                                      FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
                                      localFile.delete()
                                    }
                                    else {
                                      LOGGER.error("Local Source File Not Found")
                                    }
                                    break
      default : //do nothing
    }

  }

  /**
   *  Line is split into the columns or fields that we need to extract , and put as a record in the database.
   */
  @Param("line", "The Robinsons line that are to be split into Records")
  @Param("startIndex", "The Start postion of the field in the fixed width file Line Record")
  @Param("endIndex", "The End postion of the field in the fixed width file Line Record")
  @Returns("String with Trimmed results")
  private function getRobinsonImportColumn(line: String, startIndex: int, endIndex: int): String {
    var columnValue: String = " "
    if (line != null and line != "")     {
      columnValue = line.substring(startIndex, endIndex)
    }
    if (columnValue != null){
      columnValue = StringUtils.trim(columnValue)
    }
    return columnValue
  }


  /**
   *  Every contact on CM is searched and compared with the imported result set,
   *  According the requirement, All the severn 7 criterias Need to Match,
   *  1. FName
   *  2.LastName
   *  3.HouseNumber
   *  4.StreetName
   *  5.PostCode
   *  6.City
   *  7.Country //Removed this criteria for now.
   *
   *  The query's result set will contain only one record at most.
   *
   */
  @Param("contact", "The Robinsons line that are to be split into Records")
  @Returns("RobinsonInboundRec_Ext : Matching Record from Staging Table For opt out")
  private function returnRobinsonRecordsMatchingPostCodeAndName(contact: ABContact): RobinsonInboundRec_Ext {

    LOGGER.info("Type of Contact : " + contact?.IntrinsicType)
    LOGGER.info("PostCode of the Contact :" + contact?.PrimaryAddress?.PostalCode)
    if (contact typeis ABPerson){

      var robinsonsInboundQuery = gw.api.database.Query.make(RobinsonInboundRec_Ext)
      robinsonsInboundQuery.and(\ andCriteria -> {
         andCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#PostalCode,Relop.Equals,contact.PrimaryAddress?.PostalCode)
         andCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#FirstName,Relop.Equals,contact.FirstName)
         andCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#LastName,Relop.Equals,contact.LastName)
         andCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#Locality,Relop.Equals,contact.PrimaryAddress?.City)

        andCriteria.or (\orCriteria -> {
          orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#HouseNumber,Relop.Equals,contact.PrimaryAddress.AddressLine1)
          orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#HouseNumber,Relop.Equals,contact.PrimaryAddress.AddressLine2)
          orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#HouseNumber,Relop.Equals,contact.PrimaryAddress.AddressLine3)
        })
        andCriteria.or (\orCriteria -> {
          orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#Street,Relop.Equals,contact.PrimaryAddress.AddressLine1)
          orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#Street,Relop.Equals,contact.PrimaryAddress.AddressLine2)
          orCriteria.compareIgnoreCase(RobinsonInboundRec_Ext#Street,Relop.Equals,contact.PrimaryAddress.AddressLine3)
        })
       })

      var result = robinsonsInboundQuery.select().getAtMostOneRow()
     // var filteredResult = results.where(\elt -> elt.PostalCode.equalsIgnoreCase(contact?.PrimaryAddress?.PostalCode))

      LOGGER.info("****************** Matches Found in Result After Filtering filteredResult:" + result)
      return result
    }
    return null
  }

  /**
   *   Updates the Contact Preference depending on the status on Robinsons list with Optout confirmation date
   *   which serves as the effective date.
   *
   *  This will be removed,This logic was written assuming the staging table will be updated during every run,
   *  but as we are Importing records , this will not be required anymore
   *
   */
  @Param("contact", "The Robinsons line that are to be split into Records")
  @Param("RobinsonInboundRec_Ext", "Matching Record from Staging Table For opt out")

  private function updateRobinsonPreferenceOnContact(contact: ABContact, robinsonRecord: RobinsonInboundRec_Ext,markContactOptIn :boolean,preference :OptInOptOutPreference_Ext) {
    LOGGER.info("Entering Method : updateRobinsonPreferenceOnContact()...")
    if(markContactOptIn){
         updateContactPreferencesWithOptIn(contact,preference)
    }else {
        updateContactPreferences(contact, getDateFromddMMyyFormat(robinsonRecord.RegistrationDate),preference)
    }

  }
  /**
   *   converts String to Date
   *   Take param in "yyyyMMdd", and returns Java dates
   */

  private function getDateFromddMMyyFormat(dateString: String): Date {
    var df = new SimpleDateFormat("dd/MM/yy")
    var convertCurrentDate = dateString
    var date = new java.util.Date()
    date = df.parse(convertCurrentDate)
    return date
  }
}