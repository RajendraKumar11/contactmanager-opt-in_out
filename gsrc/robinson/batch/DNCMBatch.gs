package robinson.batch

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 22/4/16
 * Time: 12:12 PM
 * To change this template use File | Settings | File Templates.
 */
uses gw.processes.BatchProcessBase
uses java.io.File
uses java.lang.Exception
uses java.io.FileReader
uses java.io.BufferedReader
uses java.text.SimpleDateFormat
uses java.util.Date
uses java.lang.System
uses gw.api.system.PLConfigParameters
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses util.IntegrationPropertiesUtil
uses gw.api.database.Relop
uses org.apache.commons.lang.StringUtils
uses util.FilesUtil
uses util.common.CommonConstants
uses java.io.IOException

class DNCMBatch extends BatchProcessBase {

  override function checkInitialConditions(): boolean {
    return true
  }

  override function requestTermination(): boolean {
    return true

  }

  private static final var LOGGER: Logger = LoggerFactory.getLogger(DNCMBatch)
  private static final var file_name        = IntegrationPropertiesUtil.getProperty("DNCMInbound_file_name")
  private static final var source_File_path = IntegrationPropertiesUtil.getProperty("DNCMInbound_source_File_path")
  private static final var targetFile       = IntegrationPropertiesUtil.getProperty("DNCMInbound_targetFile")
  private static final var client_key       = IntegrationPropertiesUtil.getProperty("DNCMInbound_client_key")
  private static final var client_separator = IntegrationPropertiesUtil.getProperty("DNCMInbound_client_separator")
  private static final var daily_folder_pattern = IntegrationPropertiesUtil.getProperty("DNCMInbound_file_daily_folder_pattern")
  private static final var archive_pass_file_path = IntegrationPropertiesUtil.getProperty("DNCMInbound_archive_pass_file_path")
  private static final var archive_fail_file_path = IntegrationPropertiesUtil.getProperty("DNCMInbound_archive_fail_file_path")
  private static final var ftp_archive_pass_file_path = IntegrationPropertiesUtil.getProperty("DNCMInbound_source_FTPArchive_File_path")
  private static final var ftp_archive_fail_file_path = IntegrationPropertiesUtil.getProperty("DNCMInbound_source_FTPFailed_File_path")
  private static final var date_time_format   = IntegrationPropertiesUtil.getProperty("DNCMInbound_date_time_format")
  private static final var client_smb         = IntegrationPropertiesUtil.getProperty("DNCMInbound_client_smb")
  private static final var remote_domain_name = IntegrationPropertiesUtil.getProperty("DNCMInbound_remote_domain_name")
  private static final var archive_user_name  = IntegrationPropertiesUtil.getProperty("DNCMInbound_archive_user_name")
  private static final var archive_password_value = IntegrationPropertiesUtil.getProperty("DNCMInbound_archive_password_value")
  private static final var remote_user_name   = IntegrationPropertiesUtil.getProperty("DNCMInbound_remote_user_name")
  private static final var remote_password_value = IntegrationPropertiesUtil.getProperty("DNCMInbound_remote_password_value")
  // private static final var file_directory = IntegrationPropertiesUtil.getProperty("DNCMInbound_dir_locatiin")

  construct() {
    super(BatchProcessType.TC_DNCMIMPORTBATCH)
  }

  /**
   *   Batch Process Method for DNCM INBOUND BATCH
   *   1. Cleans The Staging Table
   *   2. Process the File and Adds the records into DNCMInboundRecords_Ext
   *      done by Method - processSourceFile(file_name, source_File_path, targetFile)
   *   3. Runs on all the contacts and matches with the DNCM Imported data from the staging table.
   *      DNCMInboundRecords_Ext , if Match founds, Opt ins or Opt out
   */
  override function doWork() {
    LOGGER.info("DNCMBatch :doWork : Begins ")
    var status = 0
    try {
      cleanTable()

      status = processSourceFileAndAddRecords(file_name, source_File_path, targetFile)
      if(status >= 0){
          processRecords()
      }else{
        LOGGER.error(" Cannot continue the batch process.. check logs " )
      }
    } catch (e: Exception) {
      LOGGER.error(" Exception in DNCMBatch :doWork()  method.. : " + e)
    }
    LOGGER.info("DNCMBatch :doWork() : Ends ")
  }

  /**
   * Method reads flat file and update DNCMStatusInbound_Ext entity
   * One by one the record is read and added in the staging table
   * DNCMInboundRecords_Ext , if any unexpected format ,then halts the process
   *
   * @param file  The source path from where the file is read
   */
  private function updateDatabase(file: File): int {
    LOGGER.info("DNCMBatch : updateDNCMStatusToDatabase : Begins ")
    var br: BufferedReader = null
    var errorStatus: int = 0
    try {
      br = new BufferedReader(new FileReader(file))
      var line = br.readLine()
      while (line != null && line != "") {

        if (line.NotBlank) {
          var lineRecords = line.split(";")
          if (lineRecords.length != 2){
              errorStatus = -1
              throw new Exception("Record errors unexpected file Format")
          }
          var phoneNumber = lineRecords.toList().get(0)
          phoneNumber = phoneNumber != null ? StringUtils.remove(phoneNumber, '+') : phoneNumber
          var optOutDate = lineRecords.toList().get(1)
           LOGGER.info("PhoneNumber : " + phoneNumber + "   Length===" + phoneNumber.length)
           LOGGER.info("optOutDate : " + optOutDate + "   Length===" + optOutDate.length)
          if (phoneNumber.length > 8 && optOutDate.length == 8){

            gw.transaction.Transaction.runWithNewBundle(\bundle -> {
               LOGGER.info("##################### Updating New Record")
              var dncmInboundRecords_Ext = new DNCMInboundRecords_Ext()
              bundle.add(dncmInboundRecords_Ext)
              dncmInboundRecords_Ext.Phone = phoneNumber
              dncmInboundRecords_Ext.OptOutDate = optOutDate
              dncmInboundRecords_Ext.Processed = Boolean.FALSE
            }, "su")
          }
        }
        line = br?.readLine()
      }
      errorStatus = 1
    }
    catch (ex: Exception) {
      errorStatus = - 1
      handleDNCMInboundException(ex, ex.Message)
      LOGGER.error("Error reading the flat file data for DNCM ,All records could not be added into Staging Table : " + "\\n" + ex.StackTraceAsString)
    }
    finally {
      if (br != null) {
        br.close()
      }
    }
    LOGGER.info("DNCMBatch : updateDatabase : Ends ")
    return errorStatus
  }

  /**
   * Increments Operations failed and log exception cause and stack trace
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  private function handleImportException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
    incrementOperationsFailed()
    OperationsFailedReasons.add(message)
  }

  /**
   *     Method load file frm remote location and copy it to local location
   *     and updated file content to database
   */
  private function processSourceFileAndAddRecords(fileName: String, sourceFilePath: String, targetFilePath: String) : int {
    LOGGER.info("DNCMBatch : processSourceFileAndAddRecords : Begins ")
    var status = 0
      var file: File = null
      try {
        LOGGER.info("DNCMBatch : fileName : " + fileName + ", sourceFilePath :" + sourceFilePath + ", targetFilePath :" + targetFilePath)
        // Build source path
        var sourcePath = sourceFilePath + fileName
        var targetPath = targetFilePath + fileName

        LOGGER.debug("DNCMBatch : sourcePath : " + sourcePath)
        LOGGER.debug("DNCMBatch : targetPath : " + targetPath)
        LOGGER.debug("DNCMBatch : env : " + java.lang.System.getProperty("gw.pc.env"))

        util.FilesUtil.nioCopyFromSourceToDestination(new File(sourcePath), new File(targetPath))

        file = new File(targetPath)

        if (file.canRead()) {
          LOGGER.info(" DNCMBatch  file can read")

            try {
                // Update database with generated flat file
                status = updateDatabase(file)
                LOGGER.debug("DNCMBatch : Add Record to DNCMStaging Entity :status " + status)
                //Operation Completed Successfully
                incrementOperationsCompleted()
              } catch (ex: Exception) {
                 handleDNCMInboundException(ex, "Could NOT update database For file : " + file.Name)
                 status = -1
              }

          try {
            if (status < 0) {
              moveFilesDependingOnStatus(CommonConstants.FTP_FAILED)
              return status
            } else {
              moveFilesDependingOnStatus(CommonConstants.FTP_PROCESSED)
              return status
            }
          } catch (ex: Exception) {
            status = -1
            handleDNCMInboundException(ex, "Exception in moving/deleting file " + file.AbsolutePath)
          }
        } else {
          status = -1
          throw new Exception("Cannot read the file DNCMBatch.processSourceFileAndAddRecords()" )
        }
      }
    catch (ex: IOException) {
      status = -1
      handleDNCMInboundException(ex, " Exception duringcopyFile - For file  " + fileName)
    }

    catch (ex: Exception) {
      status = -1
      handleDNCMInboundException(ex, "Exception on processSourceFileAndAddRecords() - Could NOT process file " + fileName)
    }
    LOGGER.info("RobinsonImportBatch : processSourceFileAndAddRecords() : Ends ")
    return status
  }

  /**
   *   converts String to Date
   */
  private static  function convertStringToDate(dateString: String): Date {
    var formatter = new SimpleDateFormat("yyyyMMdd")
    if (dateString != null) {
      return (formatter.parse(dateString) )
    }
    else
      return null
  }

  /**
   * Process the Records which were imported into the staging table,
   * Rather iterating through the N Imported Records over GW Contacts,
   * GW contacts are compared with the imported list, If any Match found
   * the corresponding contact is marked for updating their contact preference
   *
   * If the GW Contact's, Contact preference's last source is DNCM,
   * and the imported list doesn't have a match found then it has to be marked
   *  "opt in" and status to "Default" .
   *
   */

  private function processRecords() {
    var errorCount = 0
    LOGGER.info("In Method processRecords()")
    var foundContacts = gw.api.database.Query.make(ABContact).select()

    try {
      foundContacts?.each( \ contact ->  {

        var earlierOptedOutFlag = false

        LOGGER.info("processRecords() , Trying for Contact" + contact)

        var dncmFormattedPhoneNumberFromGWContact = findPhoneNumber(contact)
        var callPreference = contact.ContactPreferenceOptions_Ext.firstWhere( \ elt -> elt.ContactPreferenceType == typekey.ABOptInOutPreferType_Ext.TC_CALLS)
        if(callPreference.LastSourceOfUpdate == ABLastSourceOfUpdate_Ext.TC_ROBINSON){
          earlierOptedOutFlag = true
        }

        LOGGER.info("Found ?? dncmFormattedPhoneNumberFromGWContact :" + dncmFormattedPhoneNumberFromGWContact)
        if (dncmFormattedPhoneNumberFromGWContact != null){

          var matchingFromImportedDcnmContactRecord = gw.api.database.Query.make(DNCMInboundRecords_Ext)
              .compareIgnoreCase(DNCMInboundRecords_Ext#Phone, Relop.Equals, dncmFormattedPhoneNumberFromGWContact).select().getAtMostOneRow()

           LOGGER.info("********* Was matching Found " + matchingFromImportedDcnmContactRecord)

          if (matchingFromImportedDcnmContactRecord != null) {

            LOGGER.info(" matchingFromImportedDcnmContactRecord Found  ,Will try to get Update Contact Preference :" + matchingFromImportedDcnmContactRecord)

             updateDNCMCallPreferenceOnContact(contact,matchingFromImportedDcnmContactRecord,false,callPreference)

          }
          else if(earlierOptedOutFlag && matchingFromImportedDcnmContactRecord == null ){
            //This condition for Optin : Scnenario : Earlier contact marked out, but now cant find that matching details in
            //DNCM List then mark it Opt In with Default Status

            updateDNCMCallPreferenceOnContact(contact, null,true,callPreference)

          }
        }
      })
    } catch (e: Exception) {
        errorCount = -1
        throw new Exception("Record Processing Failed DNCMBatch.processSourceRecord()=" + e.Message)
    }
        finally {
          if (errorCount < 0) {
            moveFilesDependingOnStatus(CommonConstants.FAILED)
          } else {
            moveFilesDependingOnStatus(CommonConstants.SUCCESS)
          }
        }
  }

  /**
   * Increments Operations failed and log exception cause and stack trace
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  private function handleDNCMInboundException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
    incrementOperationsFailed()
    OperationsFailedReasons.add(message)
  }

  /**
   *  Cleans the Staging Table
   */
  function cleanTable() {
    var dncmInboundRecords_Ext = gw.api.database.Query.make(DNCMInboundRecords_Ext).select()
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      dncmInboundRecords_Ext.each(\elt -> {
        bundle.delete(elt)
      })
    }, PLConfigParameters.UnrestrictedUserName.Value)
  }

  /** matchPhoneNumberWithDNCMNumber()
   *  Returns true if the Contact was successfully updated  with the optOut date.
   *  The contact will not be udpdated if the status is "Internal",
   *  only "Robinsons" and "Default" status values are updated
   *
   *  The Internal being the Priority , we do not over ride , if by case there is new updated OptOut date
   *  That is considered and updated for contact preference Effective Date
   */
  @Param("DNCMNumber", "The Phone number from the DNCM LIST")
  @Param("formattedContactNumber", "The GW stores the phone number in differnt format ,and hence we format to our need and compare")
  @Returns("boolean True if the phone number match is found in DNCM vs GW Contact")
  function matchPhoneNumberWithDNCMNumber(DNCMNumber: String, formattedContactNumber: String): boolean {
    var phoneNumber = DNCMNumber

    if (DNCMNumber != null && formattedContactNumber != null){

      phoneNumber = StringUtils.remove(phoneNumber, '+')
      phoneNumber = StringUtils.remove(phoneNumber, '-')
      phoneNumber = StringUtils.deleteWhitespace(phoneNumber)

      return phoneNumber.equalsIgnoreCase(formattedContactNumber)
    }
     LOGGER.info("Could not Match the number for the DNCM Number" + DNCMNumber)
    return false
  }

  function getDate(dateString: String): Date {
    var df = new SimpleDateFormat("yyyyMMdd")
    var cnvertCurrentDate = dateString
    var date = new java.util.Date()
    date = df.parse(cnvertCurrentDate)
    return date
  }

  function getDateTimeStringForFile(dateTimeFormat: String): String {
    var dateFormat = new SimpleDateFormat(dateTimeFormat)
    var date = new java.util.Date()
    return dateFormat.format(date)
  }

  function addTimestamp(name: String, timePart: String): String {
    var lastIndexOf: int = name.lastIndexOf('.')
    return (lastIndexOf == - 1 ? name + "_" + timePart : name.substring(0, lastIndexOf) + "_" + timePart + name.substring(lastIndexOf)).replaceAll("[\\/:\\*\\?\"<>| ]", "_")
  }

  /**
   * Extracts the contact's Primary phone number in comparable fashion ,
   * we rip off the - , spaces , + and signs , and compare with DNCM,
   * This is required inorder to find the match in phone number
  */
  @Param("contact", "GW Contact which needs to be updated")
  @Returns("Formatted number by which we can compare with DNCM List")
  function findPhoneNumber(contact: ABContact): String {
    var extractedPhoneNumber = " "
    LOGGER.debug(contact.DisplayName)
    LOGGER.debug(contact.PrimaryPhone)
    LOGGER.debug(contact.IntrinsicType)
    LOGGER.info("Contacts Primary Phone Value :" + contact.PrimaryPhoneValue)

    if ((contact typeis ABPerson ) && (contact.PrimaryPhone == PrimaryPhoneType.TC_MOBILE && contact.CellPhoneCountry != null)) {
       LOGGER.info("CellPhone Country Code  :" + gw.api.util.PhoneUtil?.getCountry(contact?.CellPhoneCountry?.Code))
       LOGGER.info("CellPhone Country Code num :" + gw.api.util.PhoneUtil?.getCountryCodeForRegion(contact?.CellPhoneCountry))
      var countryNumber = gw.api.util.PhoneUtil?.getCountryCodeForRegion(contact?.CellPhoneCountry)
      var phoneNumber = contact?.CellPhone?.startsWith("0") ? contact?.CellPhone?.substring(1) :contact?.CellPhone
      var formatedPhoneNumber = countryNumber + phoneNumber
       LOGGER.info("The formatedPhoneNumber value" + formatedPhoneNumber)
      extractedPhoneNumber = formatedPhoneNumber
      return extractedPhoneNumber
    }
    else if (contact.PrimaryPhone == PrimaryPhoneType.TC_HOME && contact.HomePhoneCountry != null) {
       LOGGER.info("HomePhone Country Code  :" + gw.api.util.PhoneUtil?.getCountry(contact?.HomePhoneCountry?.Code))
       LOGGER.info("HomePhone Country Code num :" + gw.api.util.PhoneUtil?.getCountryCodeForRegion(contact?.HomePhoneCountry))

      var countryNumber = gw.api.util.PhoneUtil?.getCountryCodeForRegion(contact?.HomePhoneCountry)
      var phoneNumber = contact?.HomePhone?.startsWith("0") ? contact?.HomePhone?.substring(1) :contact?.HomePhone
      var formatedPhoneNumber = countryNumber + phoneNumber

      LOGGER.info("the formatedPhoneNumber value" + formatedPhoneNumber)
      extractedPhoneNumber = formatedPhoneNumber
      return extractedPhoneNumber
    }
    else if (contact.PrimaryPhone == PrimaryPhoneType.TC_WORK && contact.WorkPhoneCountry != null){
         LOGGER.debug("work Phone Country Code  :" + gw.api.util.PhoneUtil?.getCountry(contact?.WorkPhoneCountry?.Code))
         LOGGER.debug("work phone Country Code num :" + gw.api.util.PhoneUtil?.getCountryCodeForRegion(contact?.WorkPhoneCountry))
         LOGGER.debug("work phone Country Code num :" + contact?.WorkPhone +"for  contact"+contact.DisplayName)

        var countryNumber = gw.api.util.PhoneUtil?.getCountryCodeForRegion(contact?.WorkPhoneCountry)
        var phoneNumber = contact?.WorkPhone?.startsWith("0") ? contact?.WorkPhone?.substring(1) : contact?.WorkPhone
        var formatedPhoneNumber = countryNumber + phoneNumber

         LOGGER.info("the formatedPhoneNumber value" + formatedPhoneNumber)
        extractedPhoneNumber = formatedPhoneNumber
        return extractedPhoneNumber
      }
     LOGGER.info("===========No Phone Records found for The Contact ====")
    return null
  }

  function updateContactPreferences(contact: ABContact, optOutDate: Date,callPreference : OptInOptOutPreference_Ext): boolean {
    if ((callPreference != null ) &&
       (callPreference.LastSourceOfUpdate == null || callPreference.LastSourceOfUpdate != ABLastSourceOfUpdate_Ext.TC_INTERNAL) &&
       ( ((callPreference.LatestUpdateDateTime?.differenceInDays(optOutDate) >= 0 )
          || (callPreference.LatestUpdateDateTime ==null))
       /* ||
           (callPreference.LastSourceOfUpdate != ABLastSourceOfUpdate_Ext.TC_DEFAULT)*/

       )

    ) {

      gw.transaction.Transaction.runWithNewBundle(\b -> {
        b.add(callPreference)
        callPreference.LastSourceOfUpdate = ABLastSourceOfUpdate_Ext.TC_ROBINSON
        callPreference.LatestUpdateDateTime = optOutDate
        callPreference.Opt = ABContactPreferOption_Ext.TC_OPTOUT
      }, "su")
      LOGGER.info(" updateContactPreferences)() Successfully Updated the contact:" + contact)
      return true
    } else {
       LOGGER.info("Debug  Value : Didnt find the matching contact skipping the current :" + contact.DisplayName)
      return false
    }
  }

  private function moveFilesDependingOnStatus(fileStatus: String) {
    if (util.common.CommonConstants.SUCCESS.equalsIgnoreCase(fileStatus)) {

      var localFile: File = new File (targetFile + file_name)
      if (localFile.exists()) {
        var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
        var archievedFilePath = archive_pass_file_path + File.separator + archivedFileName;
        FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
        localFile.delete()
      }
      else {
        LOGGER.info("Local Source File Not Found")
      }
    }
    else if (util.common.CommonConstants.FAILED.equalsIgnoreCase(fileStatus)) {
      var localFile: File = new File (source_File_path + file_name)
      if (localFile.exists()) {
        var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
        var archievedFilePath = archive_fail_file_path + File.separator + archivedFileName;
        FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
        localFile.delete()
      }
      else {
        LOGGER.info("Local Source File Not Found")
      }
    } else if (util.common.CommonConstants.FTP_PROCESSED.equalsIgnoreCase(fileStatus)) {
      var localFile: File = new File (source_File_path + file_name)
      if (localFile.exists()) {
        var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
        var archievedFilePath = ftp_archive_pass_file_path + File.separator + archivedFileName;
        FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
        localFile.delete()
      }
      else {
        LOGGER.info("Local Source File Not Found")
      }
    }
    else if (util.common.CommonConstants.FTP_FAILED.equalsIgnoreCase(fileStatus)) {
        var localFile: File = new File (source_File_path + file_name)
        if (localFile.exists()) {
          var archivedFileName = addTimestamp(localFile.Name, getDateTimeStringForFile(date_time_format))
          var archievedFilePath = ftp_archive_fail_file_path + File.separator + archivedFileName;
          FilesUtil.nioCopyFromSourceToDestination(localFile.getCanonicalFile(), new File(archievedFilePath).getCanonicalFile())
          localFile.delete()
        }
        else {
          LOGGER.info("Local Source File Not Found")
        }
      }
  }

  /**
   *   Updates the Contact Preference depending on the status on Robinsons list with Optout confirmation date
   *   which serves as the effective date.
   *
   *  This will be removed,This logic was written assuming the staging table will be updated during every run,
   *  but as we are Importing records , this will not be required anymore
   *
   */
  @Param("contact", "The Robinsons line that are to be split into Records")
  @Param("DNCMInboundRecords_Ext", "Matching Record from Staging Table For opt out")

  private function updateDNCMCallPreferenceOnContact(contact: ABContact, dncmMatchingRecord: DNCMInboundRecords_Ext,markContactOptIn :boolean ,callPreference : OptInOptOutPreference_Ext) {
    LOGGER.info("Entering Method : updateRobinsonPreferenceOnContact()...")
    if(markContactOptIn){
      updateContactPreferencesWithOptIn(contact,callPreference)

    }else {

      updateContactPreferences(contact, getDate(dncmMatchingRecord.OptOutDate),callPreference )
    }

  }

  /**
   *  Returns true if the Contact was successfully updated  with the Optin date as Current Date.
   *  The contact will not be updated to "Default" if the Source is "Robinsons"
   *  and
   *  The Internal being the Priority , We set it back to
   */
  @Param("contact", "GW Contact which needs to be updated")
  @Param("optOutDate", "The Date of DNCM Registration Date for the contact")
  @Returns("True if Contact was succesfully updated")
  function updateContactPreferencesWithOptIn(contact: ABContact , callPreference : OptInOptOutPreference_Ext): boolean {
    LOGGER.info("In Method.updateContactPreferencesWithOptIn()...")

    if ( callPreference.LastSourceOfUpdate == ABLastSourceOfUpdate_Ext.TC_ROBINSON ) {

      gw.transaction.Transaction.runWithNewBundle(\b -> {
        b.add(callPreference)
        callPreference.LastSourceOfUpdate = ABLastSourceOfUpdate_Ext.TC_DEFAULT
        callPreference.LatestUpdateDateTime = new Date()
        callPreference.Opt = ABContactPreferOption_Ext.TC_OPTIN
      }, PLConfigParameters.UnrestrictedUserName.Value)
      LOGGER.info(" updateContactPreferencesWithOptIn() Successfully Updated the contact:" + contact)
      return true
    } else {
      LOGGER.info("Debug Value : Did not find the matching contact and skipping the current contact :" + contact.DisplayName)
      return false
    }
  }

}