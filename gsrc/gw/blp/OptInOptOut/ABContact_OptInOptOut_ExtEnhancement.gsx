package gw.blp.OptInOptOut

/**
 * This is an enhancement class to Contact entity which handles the OptInOptOut functionality.
 * @author Rajendra Kumar
 *
 */
enhancement ABContact_OptInOptOut_ExtEnhancement: entity.ABContact {
  /**
   * Sets the default value for the 'Opt','ContactPreferenceType' and 'Last Source of Update' fields of digital, phone or paper mail contact preferences.
   */
  function setDefaultCommercialContactPreferenceOptions() {
    for (type in typekey.ABOptInOutPreferType_Ext.getTypeKeys(false)) {
      var contactPreferenceOption = new OptInOptOutPreference_Ext()
      switch (type.DisplayName) {

        case "Digital":

            contactPreferenceOption.Opt = typekey.ABContactPreferOption_Ext.TC_UNDEFINED
            contactPreferenceOption.ContactPreferenceType = typekey.ABOptInOutPreferType_Ext.TC_DIGITAL
            this.addToContactPreferenceOptions_Ext(contactPreferenceOption)
            break

        case "Calls":

            contactPreferenceOption.Opt = typekey.ABContactPreferOption_Ext.TC_UNDEFINED
            contactPreferenceOption.ContactPreferenceType = typekey.ABOptInOutPreferType_Ext.TC_CALLS
            this.addToContactPreferenceOptions_Ext(contactPreferenceOption)
            break

        case "Mails":

            contactPreferenceOption.Opt = typekey.ABContactPreferOption_Ext.TC_UNDEFINED
            contactPreferenceOption.ContactPreferenceType = typekey.ABOptInOutPreferType_Ext.TC_MAILS
            this.addToContactPreferenceOptions_Ext(contactPreferenceOption)
            break
      }
    }
  }

  /**
   * Checks if the contact has any existing contact preference options. If yes, returns the same.
   * If not, returns the list of contact preferences with predefined default values.
   */
  @Returns("a value to indicate the method has been called")
  property get defaultCommercialContactPreferenceOptions(): List<OptInOptOutPreference_Ext> {
    if (this.ContactPreferenceOptions_Ext.Count == 0 or this.ContactPreferenceOptions_Ext == null) setDefaultCommercialContactPreferenceOptions()
    return this.ContactPreferenceOptions_Ext.toList()
  }

  /**
   * Updates contact preference fields('Last Update Date Time' and 'Last Source of Update') of digital, phone or paper mail, whenever there is a change in their respective
   * 'opt' option.
   */
  @Param("contactPreferenceOption", "the particular contact preference row, where the optin optout option was changed")
  function updateContactPreferenceFields(contactPreferenceOption: OptInOptOutPreference_Ext) {
    contactPreferenceOption.isFieldChanged("Opt")
  {

    if (contactPreferenceOption.Opt == typekey.ABContactPreferOption_Ext.TC_UNDEFINED) {
        contactPreferenceOption.LatestUpdateDateTime = null
        contactPreferenceOption.LastSourceOfUpdate = null
    }
    else{
       /*
       updates latest updates date time with current date and last source of update with 'Internal', whenever
       there 'Opt' is changed.
       */
      contactPreferenceOption.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
      contactPreferenceOption.LastSourceOfUpdate = typekey.ABLastSourceOfUpdate_Ext.TC_INTERNAL
    }

  }
  }
}
